import { Encoder } from '@msgpack/msgpack'
import { ExchangeName, ExchangePair, PairPriceUpdate } from 'exchange-models/exchange'
import { Subscribe } from 'exchange-models/kraken'
import { Logger } from 'winston'
import { Publisher } from 'zeromq'
import * as kraken from 'kraken-helpers'
import WebSocket from 'ws'

export interface Configuration {
  threshold: number
  wsUrl: string
  apiUrl: string
  broadcastPort: string
  exchangeName: ExchangeName
}

export interface Microservice {
  logger: Logger
  tickerWS: WebSocket
  tickerZ: Publisher
  isRunning: boolean
  tickCount: number
  encoder: Encoder<unknown>
}

export let createTickSubRequest: { (instruments: string[]): Subscribe }
export let isError: { (event: unknown): event is Error }
export let parseEvent: { (eventData: string): string | Error | PairPriceUpdate }
export let createStopRequest: { (): object }
export let getAvailablePairs: {
  (exchangeApiUrl: string, threshold: number): Promise<Error | ExchangePair[]>
}

export let sleep = (ms: number): Promise<unknown> => {
  return new Promise(resolve => setTimeout(resolve, ms))
}

export let selectExchange = (exchangeName: ExchangeName): void => {
  if (exchangeName === 'kraken') {
    createTickSubRequest = kraken.createTickSubRequest
    isError = kraken.isError
    parseEvent = kraken.parseEvent
    getAvailablePairs = kraken.getAvailablePairs
    createStopRequest = kraken.createStopRequest
  }
}

export let createService = (logger: Logger, conf: Configuration): Microservice | Error => {
  // create the microservice
  let ms: Microservice = {
    tickerWS: new WebSocket(conf.wsUrl),
    tickerZ: new Publisher(),
    isRunning: true,
    tickCount: 0,
    logger: logger,
    encoder: new Encoder(),
  }

  // bind ticker publisher
  ms.tickerZ.bind(conf.broadcastPort)
  ms.tickerZ.noDrop = false
  ms.tickerZ.sendHighWaterMark = 0
  ms.tickerZ.sendTimeout = -1
  if (!ms.tickerWS) return Error('Failed to create new web socket')
  return ms
}

export let getPairs = async (conf: Configuration): Promise<string[] | Error> => {
  // attempt to get available pairs
  let response = await getAvailablePairs(conf.apiUrl, conf.threshold)
  if (response instanceof Error) return response
  return response.map(p => p.tradename)
}
