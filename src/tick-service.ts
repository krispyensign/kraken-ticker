import * as c from './common'

let shutdown = async (ms: c.Microservice | Error): Promise<void> => {
  if (ms instanceof Error) return
  // unsubscribe from ticker
  ms.logger.info('Got shutdown request')
  ms.tickerWS.send(JSON.stringify(c.createStopRequest()))
  await c.sleep(10)
  ms.tickerWS.close()
  ms.tickerZ.close()
  ms.logger.info('Shutdown complete')
  ms.isRunning = false
}

let processTick = async (eventData: string, ms: c.Microservice | Error): Promise<boolean> => {
  // short-circuit if shutting down
  if (ms instanceof Error || !ms.isRunning) return false

  // attempt to parse the event
  ms.logger.debug(eventData)
  let event = c.parseEvent(eventData)

  // log then eat any errors
  if (c.isError(event)) {
    ms.logger.error(event)
    ms.logger.error(eventData)
    return false
  }

  ms.tickCount++
  if (ms.tickCount === 4) {
    ms.logger.info('Ticker is healthy.  Waiting on shutdown')
  }

  // wait publisher is available
  while (!ms.tickerZ.writable) await c.sleep(2)
  await ms.tickerZ.send([ms.encoder.encode(event)])
  return true
}

export let tickService = async (
  conf: c.Configuration,
  ms: c.Microservice
): Promise<Error | undefined> => {
  // configure ticker behavior
  c.selectExchange(conf.exchangeName)

  let pairs = await c.getPairs(conf)
  if (pairs instanceof Error) return pairs

  // register handlers
  ms.tickerWS.on('message', async (eventData: string) => await processTick(eventData, ms))
  process.once('SIGINT', async () => await shutdown(ms))

  // sleep while waiting for client socket state
  while (ms.tickerWS.readyState === 0) await c.sleep(100)
  ms.logger.info('Socket ready state: ' + ms.tickerWS.readyState)

  // subscribe
  ms.tickerWS.send(JSON.stringify(c.createTickSubRequest(pairs)))
  ms.logger.info('Subscription request sent')

  // run until service shutdown
  while (ms.isRunning) await c.sleep(10)
  return undefined
}
