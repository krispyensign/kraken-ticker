// eslint-disable-next-line @typescript-eslint/no-var-requires
require('source-map-support').install()
import * as conf from './config.json'
import { LoggerFactoryService } from 'socket-comms-libs'
import yargs = require('yargs/yargs')
import { tickService } from './tick-service'
import { Configuration, createService } from './common'

const argv = yargs(process.argv.slice(2)).options({
  exchangeName: { type: 'string', default: conf.exchange },
  threshold: { type: 'number', default: conf.threshold },
  wsUrl: { type: 'string', default: conf.wsUrl },
  broadcastPort: { type: 'string', default: conf.broadcastPort },
  apiUrl: { type: 'string', default: conf.apiUrl },
}).argv

// create and init the microservice
let ms = createService(new LoggerFactoryService().getLogger('TickerService'), argv as Configuration)
if (ms instanceof Error) throw ms

tickService(argv as Configuration, ms)
