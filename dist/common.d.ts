import { Encoder } from '@msgpack/msgpack';
import { ExchangeName, ExchangePair, PairPriceUpdate } from 'exchange-models/exchange';
import { Subscribe } from 'exchange-models/kraken';
import { Logger } from 'winston';
import { Publisher } from 'zeromq';
import WebSocket from 'ws';
export interface Configuration {
    threshold: number;
    wsUrl: string;
    apiUrl: string;
    broadcastPort: string;
    exchangeName: ExchangeName;
}
export interface Microservice {
    logger: Logger;
    tickerWS: WebSocket;
    tickerZ: Publisher;
    isRunning: boolean;
    tickCount: number;
    encoder: Encoder<unknown>;
}
export declare let createTickSubRequest: {
    (instruments: string[]): Subscribe;
};
export declare let isError: {
    (event: unknown): event is Error;
};
export declare let parseEvent: {
    (eventData: string): string | Error | PairPriceUpdate;
};
export declare let createStopRequest: {
    (): object;
};
export declare let getAvailablePairs: {
    (exchangeApiUrl: string, threshold: number): Promise<Error | ExchangePair[]>;
};
export declare let sleep: (ms: number) => Promise<unknown>;
export declare let selectExchange: (exchangeName: ExchangeName) => void;
export declare let createService: (logger: Logger, conf: Configuration) => Microservice | Error;
export declare let getPairs: (conf: Configuration) => Promise<string[] | Error>;
