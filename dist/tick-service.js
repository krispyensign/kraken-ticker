"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.tickService = void 0;
const c = __importStar(require("./common"));
let shutdown = async (ms) => {
    if (ms instanceof Error)
        return;
    // unsubscribe from ticker
    ms.logger.info('Got shutdown request');
    ms.tickerWS.send(JSON.stringify(c.createStopRequest()));
    await c.sleep(10);
    ms.tickerWS.close();
    ms.tickerZ.close();
    ms.logger.info('Shutdown complete');
    ms.isRunning = false;
};
let processTick = async (eventData, ms) => {
    // short-circuit if shutting down
    if (ms instanceof Error || !ms.isRunning)
        return false;
    // attempt to parse the event
    ms.logger.debug(eventData);
    let event = c.parseEvent(eventData);
    // log then eat any errors
    if (c.isError(event)) {
        ms.logger.error(event);
        ms.logger.error(eventData);
        return false;
    }
    ms.tickCount++;
    if (ms.tickCount === 4) {
        ms.logger.info('Ticker is healthy.  Waiting on shutdown');
    }
    // wait publisher is available
    while (!ms.tickerZ.writable)
        await c.sleep(2);
    await ms.tickerZ.send([ms.encoder.encode(event)]);
    return true;
};
let tickService = async (conf, ms) => {
    // configure ticker behavior
    c.selectExchange(conf.exchangeName);
    let pairs = await c.getPairs(conf);
    if (pairs instanceof Error)
        return pairs;
    // register handlers
    ms.tickerWS.on('message', async (eventData) => await processTick(eventData, ms));
    process.once('SIGINT', async () => await shutdown(ms));
    // sleep while waiting for client socket state
    while (ms.tickerWS.readyState === 0)
        await c.sleep(100);
    ms.logger.info('Socket ready state: ' + ms.tickerWS.readyState);
    // subscribe
    ms.tickerWS.send(JSON.stringify(c.createTickSubRequest(pairs)));
    ms.logger.info('Subscription request sent');
    // run until service shutdown
    while (ms.isRunning)
        await c.sleep(10);
    return undefined;
};
exports.tickService = tickService;
//# sourceMappingURL=tick-service.js.map