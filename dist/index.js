"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
// eslint-disable-next-line @typescript-eslint/no-var-requires
require('source-map-support').install();
const conf = __importStar(require("./config.json"));
const socket_comms_libs_1 = require("socket-comms-libs");
const yargs = require("yargs/yargs");
const tick_service_1 = require("./tick-service");
const common_1 = require("./common");
const argv = yargs(process.argv.slice(2)).options({
    exchangeName: { type: 'string', default: conf.exchange },
    threshold: { type: 'number', default: conf.threshold },
    wsUrl: { type: 'string', default: conf.wsUrl },
    broadcastPort: { type: 'string', default: conf.broadcastPort },
    apiUrl: { type: 'string', default: conf.apiUrl },
}).argv;
// create and init the microservice
let ms = common_1.createService(new socket_comms_libs_1.LoggerFactoryService().getLogger('TickerService'), argv);
if (ms instanceof Error)
    throw ms;
tick_service_1.tickService(argv, ms);
//# sourceMappingURL=index.js.map