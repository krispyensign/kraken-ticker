"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getPairs = exports.createService = exports.selectExchange = exports.sleep = exports.getAvailablePairs = exports.createStopRequest = exports.parseEvent = exports.isError = exports.createTickSubRequest = void 0;
const msgpack_1 = require("@msgpack/msgpack");
const zeromq_1 = require("zeromq");
const kraken = __importStar(require("kraken-helpers"));
const ws_1 = __importDefault(require("ws"));
let sleep = (ms) => {
    return new Promise(resolve => setTimeout(resolve, ms));
};
exports.sleep = sleep;
let selectExchange = (exchangeName) => {
    if (exchangeName === 'kraken') {
        exports.createTickSubRequest = kraken.createTickSubRequest;
        exports.isError = kraken.isError;
        exports.parseEvent = kraken.parseEvent;
        exports.getAvailablePairs = kraken.getAvailablePairs;
        exports.createStopRequest = kraken.createStopRequest;
    }
};
exports.selectExchange = selectExchange;
let createService = (logger, conf) => {
    // create the microservice
    let ms = {
        tickerWS: new ws_1.default(conf.wsUrl),
        tickerZ: new zeromq_1.Publisher(),
        isRunning: true,
        tickCount: 0,
        logger: logger,
        encoder: new msgpack_1.Encoder(),
    };
    // bind ticker publisher
    ms.tickerZ.bind(conf.broadcastPort);
    ms.tickerZ.noDrop = false;
    ms.tickerZ.sendHighWaterMark = 0;
    ms.tickerZ.sendTimeout = -1;
    if (!ms.tickerWS)
        return Error('Failed to create new web socket');
    return ms;
};
exports.createService = createService;
let getPairs = async (conf) => {
    // attempt to get available pairs
    let response = await exports.getAvailablePairs(conf.apiUrl, conf.threshold);
    if (response instanceof Error)
        return response;
    return response.map(p => p.tradename);
};
exports.getPairs = getPairs;
//# sourceMappingURL=common.js.map